## Repositorio de Recursos Educativos de Programación II

En este proyecto se encuentran detallados los contenidos utilizados para el dictado de la materia "Programación II" durante el primer cuatrimestre del ciclo lectivo 2020 de la TUSL.    

Se encuentra organizado de la siguiente forma:
* en el directorio "Temas" están descriptos los contenidos de cada una de la unidades temáticas que conforman la materia, a excepción del Tema 5 que se dedicó exclusivamente a la definición de consignas del único Trabajo Práctico Obligatorio;    
* en el directorio "Actividades" se encuentran detalladas las tareas obligatorias alojada en Moodle correspondientes a cada unidad temática.   

Como archivo descargable se encuentra la planificación anual presentada a finales del 2019.
