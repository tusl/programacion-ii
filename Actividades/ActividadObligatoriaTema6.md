## Actividad Obligatoria Tema 6
Para la actividad práctica de este tema deben realizar al menos 3 (tres) modificaciones en los modelos de sus aplicaciones y efectuar las respectivas migraciones.

Como respuesta deben adjuntar o la captura de pantalla de la terminal o bien el código copiado y pegado explicando cuáles cambios realizaron y cómo saben que son correctos, de acuerdo con lo que les devolvió la consola de Django.