## Actividad Obligatoria

En esta actividad deberán responder las preguntas teniendo en cuenta todos los materiales que se ofrecieron en esta unidad. También pueden hacer búsquedas propias de ser necesario. Deben obtener un 80% de respuestas correctas para aprobar la actividad y cuentan con 5 intentos para lograrlo.

**Pregunta 1 - Conceptos generales** - ¿Que es un framework y a qué tipo de desarrollo está orientado Django? (20%)    
**Pregunta 2 - Sobre Python** - ¿Por qué se dice que Python es un lenguaje semicompilado y cuáles son las ventajas de que exista una gran comunidad que utiliza este lenguaje y este framework? (20%)    
**Pregunta 3 - Ventajas de Django** - ¿Cuál esa una de las principales ventajas que incluye Django con relación a facilitar el ingreso de usuarios y la actualización de contenidos de nuestra web? (20%)    
**Pregunta 4 - Arquitecturas** - ¿Cuál es la diferencia entre la arquitectura MVC de los frameworks en general y el MTV de Django? (20%)     
**Pregunta 5 - Relación con bases de datos** - Investiga brevemente qué es y en qué consiste el ORM. ¿Cuál te parece que es el principal rol que tendrá dentro del framework? (10%)     
**Pregunta 6 - Principios y formas de desarrollar** - ¿Qué significa en español la sigla DRY y qué implica en términos de desarrollo? (10%)