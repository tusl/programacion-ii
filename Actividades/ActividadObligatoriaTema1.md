## Actividad Obligatoria Tema 1
Deben realizar algo muy similar a las actividades del curso online. Les recomiendo que lean detalladamente la consigna y que antes de enviar sus respuestas prueben el código SQL en la interfaz que provee Khan Academy.    

### Primera parte
Supongamos que una de las aplicaciones que tendrá nuestra página web es una encuesta con dos preguntas sobre adicción al tabaco y realización de algún deporte.
La primera pregunta puede ser, por ejemplo, _"¿realiza en la actualidad algún deporte?"_ con las opciones de respuesta _"Sí"_ y _"No"_. La segunda pregunta podría ser _"¿A qué edad dejó de fumar?"_ y puedo asignarle varias opciones: _"Antes de los 30"_, _"Antes de los 50"_ o _"Todavía fumo"_. 
Pueden realizar una encuesta con la pregunta que prefieran, siempre y cuando tenga **como mínimo 2 (dos) variables y una de las preguntas debe tener como mínimo 3 (tres) opciones de respuesta**.    

### Consigna:
a) Escriba el código necesario para crear la tabla con los registros y columnas de tu encuesta.
b) Inserte al menos 20 registros aleatorios para esa tabla.    
c) Teniendo en cuenta el ejemplo realice una consulta que muestre la cuenta de cada respuesta a la pregunta sobre dejar de fumar (la que tiene 3 opciones o más).     
d) Teniendo en cuenta el ejemplo realice una consulta donde se muestren los registros sólo de quienes respondieron "No" a la pregunta sobre deportes (la otra variable).    

