## Actividad Obligatoria Tema 7    

### Usando git para crear y clonar repositorios    

En esta actividad deberán realizar la [guía sencilla sobre Git](http://rogerdudler.github.io/git-guide/index.es.html "Guía Sencilla git") para iniciar un repositorio local y luego deberán elegir alguna de las siguientes opciones:    

- [Subir un repositorio a GitLab](https://cirelramos.blogspot.com.ar/2015/08/manejar-proyectos-con-gitlab-subir.html "Subir repo a Gitlab");    
- [Subir un repositorio a GitHub](https://medium.com/laboratoria-how-to/git-y-sus-amigos-los-conflictos-ed944c105cae "Subir repo a GitHub").    

Para conocer las diferencias entre GitLab y Github les invito a leer este artículo: "[GitLab: el GitHub para tus propios servidores](https://platzi.com/blog/que-es-gitlab/ "GitLab vs GitHub")".    

Pueden subir cualquier repositorio que quieran en su cuenta de GitHub o GitLab y me tienen que dejar el link al repositorio como respuesta a esta Actividad Práctica.    
_Sugerencia: podrían aprovechar e iniciar un repositorio en la carpeta donde están las aplicaciones que estuvieron desarrollando con el tutorial de Django._ 