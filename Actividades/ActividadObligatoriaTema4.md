## Actividad Obligatoria Tema 4    

En esta breve unidad les di apenas algunas definiciones que les servirán para leer mejor el código de Django.    

A continuación les propongo algunas actividades para que investiguen en la documentación oficial y completen ustedes el contenido teórico de este tema, a medida que van conociendo un poco más cómo se maneja el framework.    

**Pregunta 1: respuesta simple - Encapsulamiento** - Sí, ya sé que no está desarrollado este tema, pero se trata de una respuesta que es sencilla y podrán resolver fácilmente haciendo una pequeña investigación. Dijimos en el punto 1 sobre las Clases, que en Python los métodos y atributos son públicos. Sin embargo, hay una forma de indicar que dichos métodos y atributos no deberían usarse fuera de una clase. Investiguen cuál es esa forma. (Ayuda: se trata simplemente de un caracter tipográfico)   

Respuesta correcta: guiones bajos _     

**Pregunta 2: Opción múltiple - Estilo de código** - ¿Cómo se llama el estilo de código que sigue Python?

Opciones:    
* CamelCase
* PEP8->correcta
* lower case with underscores     

**Pregunta 3: cuadro de texto - Más métodos** - De acuerdo con la documentación oficial de Django, ¿cómo se denominan los métodos de las clases TemplateView y RedirectView que NO son heredados de la clase View?     

Respuesta correcta: get_context_data() get_redirect_url()    

**Pregunta 4: opción múltiple con dos respuestas correctas - Proyectos y aplicaciones** - Cuando comienzo un proyecto en Django, dentro de ese proyecto puedo desarrollar diferentes aplicaciones que convivan y reutilicen fragmentos de código gracias a este framework. Cada una de esas aplicaciones está asociada a... (dos respuestas correctas)    

Opciones:    
* Método    
* Modelo ->correcta    
* Paquete de Python->correcta    
* Carpeta    

**Pregunta 5: Verdadero o Falso - Lenguajes de programación** - Todos los lenguajes de programación que son orientados a objetos deben tener una clase principal para compilar correctamente   

Respuesta: Falso    
