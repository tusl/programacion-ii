## Tema 4
En este pequeño capítulo sobre apuntes teóricos sobre Django y la Programación Orientada a Objetos les propongo que acompañen las actividades que vienen realizando con el framework para identificar algunos conceptos importantes y comenzar a entender por qué decimos que en Python todo es un objeto.    
Una breve explicación acerca del paradigma de Programación Orientada a Objetos (POO) diría que se trata precisamente de **una forma de ordenar el código** utilizando conceptos como **clases, instancias, atributos y métodos**. No es algo particular de un lenguaje, sino que en algunos de ellos es más fácil seguir los lineamientos de este orden en el código. Por ejemplo: en el lenguaje Java es imprescindible que cada programa tenga una clase principal, mientras que en Python eso no es un requerimiento.    
De todas formas, entiendo que ya han visto algunos conceptos básicos acerca de POO en la asignatura Programación I, por lo cual sólo haré un repaso rápido con la ayuda de una metáfora: los planos y las construcciones.    
Cuando se confeccionan los planos, se establecen cuestiones generales acerca de la construcción: dimensiones totales, distribución de los espacios y dimensiones de cada uno de ellos, altura, cantidad y tipo de aberturas, etc. Sin embargo el plano **no constituye en sí mismo** ninguna construcción. Es solo un _modelo_. Ocurre algo similar con las clases ya que no son _objetos en sí_, sino que delimitan cuáles serán los **atributos** (también conocidos como variables) y métodos (o funciones) que la definen como clase. Dichos atributos y métodos determinan -por lo tanto- las cualidades que tendrán cada una de las instancias (objetos) pertenecientes a dicha clase.    
Ahora bien, ¿cómo podemos ver eso en Django?    

### Clases, instancias y encapsulamiento en Python    
Como ya lo dijimos, Django es un framework escrito en Python. Entender cómo funcionan las clases, instancias y encapsulamientos en este lenguaje nos posibilitará ver con mayor claridad cómo está estructurado y cómo funciona Django.    

**Clases**</p>
Para definir una clase en Python utilizamos la instrucción `class` seguida del nombre de dicha clase de objetos. En Django tenemos tres clases principales que proveen las funcionalidades para generar las vistas y que utilizaremos para ejemplificar en este capítulo: `View`, `TemplateView` y RedirectView. Cada una de esas clases posee **métodos y atributos**, algunos de los cuales son compartidos gracias a que en Python por defecto los métodos y atributos son públicos. (Más adelante veremos que existe una forma de indicar que esos métodos o atributos _no deberían usarse_ por fuera de la clase en la que se definieron). Como habrán notado, por una cuestión de estilo de código las clases se nombran siguiendo la norma [CamelCase](http://es.wikipedia.org/wiki/CamelCase). Veremos que los atributos y métodos utilizan otros estilos, lo cual es bueno saber porque nos facilita a mediano plazo la lectura del código.    

**Métodos**    
Son similares a lo que en otro contexto denominaríamos funciones, es decir: un conjunto de instrucciones definidas dentro de una función con un nombre específico, a la cual debe _"llamarse"_ y a la que se le pueden pasar ciertos argumentos. En el caso de las clases que vimos en el punto anterior, los tres métodos de la clase `View` son `dispatch()`, `http_method_not_allowed()` y `options()`. Los dos primeros métodos también se encuentran en `TemplateView` y en `RedirectView`, por lo cual podemos inferir que éstas últimas _heredan_ métodos y/o atributos de la clase `View`.

**Atributos**     
Básicamente son variables que pueden pertenecer a una o varias clases. Siguiendo con el ejemplo de la clase `View`, el único atributo que tiene es `http_methods_name` el cual aceptará sólo ciertos argumentos.     

Como ya habrán notado, tanto los métodos como los atributos utilizan un estilo distinto para identificar sus nombres. Este estilo es conocido como *lower_case_with_underscores*. La documentación completa acerca de este estilo de código se encuentra [en este sitio (en inglés)](https://www.python.org/dev/peps/pep-0008/).    
Ahora les recomiendo que vuelvan a ver los archivos de configuración que editaron para desarrollar el tutorial y traten de identificar las entidades que hemos visto hasta aquí, así se preparan para completar la Actividad obligatoria de la Unidad.     

### Algunas aclaraciones importantes     
En este apartado lo que intentamos es relacionar algunos fragmentos de código presentes en cualquier aplicación web desarrollada en Django con conceptos provenientes de la Programación Orientada a Objetos.     
Como comentamos al principio de la unidad, el objetivo es aprender haciendo y por eso verán que el énfasis está puesto en la práctica y el trabajo de desarrollo que deben realizar fuera de la plataforma. Entendemos que esta metodología les proveerá los conocimientos indispensables para distinguir los paradigmas de la programación entendiendo cómo están hechos los desarrollos, sabiendo que la teoría de patrones de diseño orientados a objetos poseen una complejidad que excede las posibilidades y los alcances de esta asignatura dentro de la Tecnicatura.    
Sin embargo, me pareció importante que se tomen el tiempo para repasar cosas teóricas mirando sus propias producciones y así tratar de comprender las principales características de la POO.    
No olviden que pueden dejar todas sus inquietudes en el Foro de este Tema y ya pueden pasar a completar la Actividad Obligatoria de la Unidad.    

