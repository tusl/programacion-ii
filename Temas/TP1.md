## Trabajo Práctico N° 1    

Como les había comentado al comienzo del ciclo en la planificación, para regularizar la materia deben realizar un trabajo práctico integrador que consiste en el desarrollo de una aplicación en Django.    

### Consigna    

Para la primera parte de este trabajo, deben realizar la planificación y detallar las especificaciones que tendrá su proyecto, respondiendo a las siguientes consignas:    

1. Definir el objetivo general que cumplirá la aplicación, cuál será su utilidad concreta. En caso que les parezca útil o ya lo hayan pensado, colocarle un nombre a la aplicación.    
2. Describir un contexto real o hipotético donde se podría desplegar la aplicación, identificando requerimientos técnicos, perfil de usuarix, público objetivo, tipo de información que utilizará el programa, etc.     
3. Enumerar al menos 3 (tres) necesidades que cubriría o funcionalidades que beneficiarían a quienes utilicen la aplicación.     
4. Diseñar el flujo de trabajo que seguirían para partir desde las necesidades detectadas hasta la etapa de prueba de la aplicación que desean desarrollar.     
5. Justifiquen por qué consideran que Django es la herramienta adecuada para desarrollar esa aplicación.    

### Formato     

El documento no deberá exceder las 3 (tres) carillas en fuente 12 con interlineado de 1,5 y hoja tamaño A4. El archivo debe enviarse en formato .odt o .pdf únicamente a través de esta plataforma.    
Pueden enviar sus consultas sobre este trabajo al Foro General de la materia.
