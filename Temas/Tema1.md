## Tema 1
Vamos a comenzar aprendiendo qué son y cómo funcionan las bases de datos porque éste concepto constituye un rasgo distintivo en los contenidos de Programación II respecto de anteriores materias. También porque si tenemos en claro la estructura y el funcionamiento de las bases de datos nos resultarán obvias las ventajas del framework sobre el cual vamos a trabajar.    
En general, podemos decir que una base de datos es conjunto de información almacenado de forma sistemática, de manera que esos datos puedan ser consultados para su posterior uso.    
Nos detendremos solamente en las bases de datos digitales, organizadas con una estructura que mediante el uso de ciertos programas de software nos posibilita acceder a la información y realizar operaciones sobre los datos que allí se almacenan.    
Existen múltiples criterios para clasificar las bases de datos:    
0) Según la variabilidad de los datos (estáticas o dinámicas)    
1) Según el contenido (bibliográficas, directorios, de información científica, etc.)    
2) Según el modelo de administración de datos (jerárquicas, de red, transaccionales, relacionales, deductivas, etc.)    

Para nuestras actividades utilizaremos **bases de datos relacionales**, que se caracterizan por contar con una estructura donde los datos se agrupan formado tablas compuestas por registros (filas) y campos (columnas).    
A continuación, empezamos a trabajar de manera interactiva aprovechando un [MOOC (Masive Online Open Course)](https://es.wikipedia.org/wiki/Massive_Open_Online_Course "mooc") disponible en Khan Academy.    
A continuación les dejo el link para acceder al curso ["Introducción a SQL: consulta y gestión de los datos"](https://es.khanacademy.org/computing/computer-programming/sql#sql-basics "Curso SQL"), que se encuentra disponible en la Khan Academy.    
El objetivo es que realicen las actividades propuestas en ese curso **sólo hasta el capítulo "Consultas relacionales en SQL"**, donde es importante que entiendan cómo **unir tablas con JOIN**.    
Para nuestro trabajo en esta materia sólo necesitamos llegar hasta ese punto del curso online, pero sería muy bueno que se animen a completarlo ;)    
