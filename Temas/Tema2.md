### Tema 2
Para el desarrollo de este tema/unidad he seleccionado algunos materiales obligatorios que son los que deben recorrer para poder resolver la actividad obligatoria del módulo.    
Lo que veremos a continuación es:

* definición de lo que es un framework con ejemplos referidos a múltiples lenguajes;    
* principales características técnicas de Django y motivos que lo conviertieron en un framework tan popular;     
* primera parte del tutorial que nos ayudará a instalar todo lo necesario para correr el entorno de desarrollo;    
* creación de nuestro primer proyecto en Django.    

Para desarrollar estos ejes les comparto este video donde se explican los principales aspectos que hacen de Django un framework muy completo y actualizado.    
Además, van a poder ver un tutorial paso a paso para instalar las dependencias necesarias antes de correr Django en sus computadoras. También tendrán una descripción detallada sobre cómo crear entornos virtuales y por qué son importantes para los entornos de desarrollo.    
Es necesario que lleguen a ver **hasta el minuto 23 del video**, luego se describen algunas cuestiones sobre editores de texto que no son importantes para nuestra tarea. Supongo que ustedes ya tienen un editor de su preferencia que habrán utilizado en materias anteriores, pero si no es así pueden ver el video completo y tener más herramientas para elegir uno.    
**Aclaración importante:** este video forma parte de una serie de tutoriales que no vamos a utilizar en esta materia porque se realizan sobre la versión 1.11 de Django. Esa versión perderá el soporte extendido en abril de este año y por ese motivo vamos a utilizar la versión 2.2 de Django. Todo lo referido a Python tiene vigencia porque está explicando cómo instalar Python 3.x y correr entornos virtuales con eso.    

<iframe src="https://www.youtube.com/embed/LD5MzX2GHZw?rel=0" allow="autoplay; encrypted-media" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>


Como complemento de lo que desarrolla Diego Rodriguez en su video, les dejo los siguientes contenidos que -si bien en esta oportunidad se refieren al framework con el que trabajaremos- aportan criterios que trascienden el lenguaje y las herramientas porque les pueden acompañar en otros contextos (especialmente lo referido a la documentación, la terminal y la comunidad.   

### Documentación
Para cualquier lenguaje de programación que hayan aprendido, que quieran aprender, o para profundizar lo que ya saben siempre es importante revisar la documentación existente al respecto. Leer la documentación puede convertirse en algo tedioso, sobre todo porque suele estar redactada y organizada con criterios explícitamente técnicos. Sin embargo, es el lugar donde van a encontrar estandarizadas la mayor parte de las funciones y utilidades de las herramientas. Por eso es recomendable recurrir en primer lugar a la documentación para poner en contexto lo que necesitamos usar y entender por qué a veces lo que creímos que debía funcionar de cierta forma termina funcionando de otra. Es muy importante verificar que estamos recurriendo a la documentación relativa a la versión del lenguaje/programa que estamos utilizando, ya que suele haber diferencias importantes en los cambios que se realizan entre versiones.   
Aquí algunos enlaces:
- [Documentación de Django versión 1.11](https://docs.djangoproject.com/es/1.11/intro/install/)    
- [Documentación de Python 3](http://docs.python.org.ar/tutorial/3/real-index.html)    

### La terminal
Elegir usar sistemas operativos libres implica amigarnos con la terminal. Sabemos que al principio cuesta, que da miedo no saber exactamente qué estamos haciendo, pero también podemos aprovechar que la terminal nos provee de información para tomar decisiones y entender nuestros errores.    
En particular, la terminal de Python es muy amigable en ese sentido. A través de los famosos _"Traceback"_ nos permite conocer no sólo el origen del error, sino que a veces nos muestra exactamente dónde se encuentra la falla. Por eso es muy importante prestar atención a lo que nos devuelve la terminal, aún cuando no entendamos del todo qué nos está diciendo. La mejor forma de encontrar la solución a los fallos es buscar en foros o en sitios web ese error que nos devolvió la terminal. Lo más probable es que ya le haya pasado a alguien más y que encontremos varias publicaciones con las soluciones a esos errores.    

### La comunidad
Relacionado con lo que les comenté en el apartado anterior, sabemos que una de las enormes ventajas del Software Libre es la amplia comunidad que lo sostiene y que acrecienta la información disponible. Cada herramienta, cada lenguaje de programación, cada framework cuenta habitualmente con sus foros de consulta y existen también "Temas" (_topics_ en inglés) dentro de foros generales de desarrollo de software que nos permiten buscar y encontrar con mayor facilidad las dudas o consultas sobre nuestros errores. Lo ideal es **siempre buscar antes de preguntar**. Es una de las primeras reglas de cualquier comunidad: antes que nada debemos asegurarnos que la respuesta que buscamos no está en ninguna pregunta anterior. De esa forma se evita no sólo duplicar información sino también hacer economía de recursos dentro de las comunidades, ya que muchas veces el trabajo de moderación y respuesta en los foros se realiza de manera voluntaria.    

### Acerca de Django
De la misma manera que debemos tener siempre presente en cuál directorio estamos realizando cambios e instalaciones dentro de nuestro sistema operativo, en Django se debe prestar especial atención a la ubicación en la que creamos el entorno virtual, ya que será allí donde encontraremos el árbol de directorios y archivos que dan vida a cada proyecto o aplicación. Se trata de un entorno muy poderoso pero también muy grande, con muchas "ramificaciones" que pueden hacernos perder muy fácilmente.    
Es sobre todo una cuestión de práctica (como todo lo relacionado a la programación) pero nos ahorrará mucho tiempo y dolores de cabeza el hecho de tener bien presente dónde estamos cada vez que ejecutamos un comando o creamos un directorio. Recordar la ubicación del entorno virtual y la diferencia entre proyecto y aplicaciones que lo integran es la clave para no perder tiempo buscando errores o averiguando dónde dejamos ese directorio que contiene la aplicación sobre la cual debemos trabajar.    



