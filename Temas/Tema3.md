## Tema3

### Introducción
Durante las próximas dos semanas nos vamos a dedicar a realizar [la primera parte del tutorial de Django en español disponible en el sitio oficial del framework](https://docs.djangoproject.com/es/2.2/intro/tutorial01/). Esto quiere decir que van a comenzar a desarrollar su primera aplicación web en Django. ¡Felicidades!    
Comenzar este desarrollo les permitirá no sólo poner a prueba sus habilidades en programación sino que también dará el puntapié inicial para que podamos ver con detenimiento algunos conceptos de la Programación Orientada a Objetos durante la próxima semana.    
Este tutorial que vamos a seguir corresponde a la versión 2.2 de Django, que es LTS (Long-Term Support).    
Les recomiendo que utilicen el foro de esta unidad para compartir sus experiencias en cuanto a la instalación del framework y cualquier duda que vaya surgiendo a medida que cumplen los puntos del tutorial.    
Ahora sí, ¡a codear!    

### Pregunta para poder avanzar en los contenidos:
**¿Pudiste instalar Django con todas sus dependencias?**    
Esta pregunta es solamente para tener un seguimiento de sus experiencias con la instalación del framework. Les pido que respondan para tener un registro de sus avances y/o dificultades. ¡Gracias!

Opciones de respuesta:
[ ] Sí, ya tengo todo preparado.    
[ ] Sí, pero todavía no verifiqué si funciona    
[ ] No, no entendí el tutorial    
[ ] No, tengo problemas con las dependencias    

### Tutorial breve para instalar Django
Les armé un tutorial breve para instalar Django dentro de un entorno virtual, ya que en el video que vieron en el tema anterior utilizan una forma un poco más compleja (clonar un repositorio) y no explica cómo crear e iniciar un virtualenv aprovechando directamente las utilidades de Python.    

#### ¿Qué es un entorno virtual?
Funciona como si fuera una capa que aísla un proyecto determinado de todo el resto de proyectos que desarrollen dentro de su computadora. Es decir: nos permite instalar las herramientas necesarias y cambiar todas las configuraciones que hagan falta durante la etapa de desarrollo de una aplicación sin afectar los otros proyectos en los cuales estamos trabajando.    
Esta ventaja nos la brinda el paquete de Python 3 denominado `venv`, mediante el cual podemos crear la cantidad de entornos virtuales necesarios para cada uno de los proyectos que encaremos, sin necesidad de tener todos estos entornos activos. De esta forma aislamos los proyectos, consumimos menos recursos en nuestras computadoras y en cada uno de esos proyectos instalamos las dependencias necesarias.    
Para comenzar con nuestra aplicación en Django, decidiremos primero dónde se va a ubicar dentro del directorio de nuestro equipo y allí crearemos el entorno virtual que la contendrá. Por ejemplo: si quiero poner mi proyecto _"proyectodjango"_ (pueden poner el nombre que prefieran) en la home de mi usuario, utilizaré el siguiente comando:    

```
anita@compu:~$ mkdir proyectodjango
```

Cambio al directorio:   

```
anita@compu:~$ cd proyectodjango
```

Para **crear** el entorno virtual _"djangoproject"_ (pueden ponerle nombre a su gusto) usamos el siguiente comando:    

```
anita@compu:~$ python3 -m venv djangoproject
```

El comando anterior solamente crea el entorno, por lo cual no van a ver ninguna diferencia en la terminal. Para **activarlo**, deben utilizar el siguiente comando:    

```
anita@compu:~$ source djangoproject/bin/activate
```
Nótese que _"djangoproject"_ debe reemplazarse por el nombre que hayan elegido para su entorno virtual.    
Una vez que activen el entorno virtual, van a notar que algo ha cambiado en la terminal: delante del prompt aparecerá entre paréntesis el nombre del *virtualenv*.    
Allí es donde deberemos instalar Django, haciendo primero una actualización del paquete `pip` y luego la instalación de Django en una versión igual o superior (por eso usamos ~=) a 2.2, que es la última versión LTS:    

```
(djangoproject)anita@compu:~$ pip install --upgrade pip
(djangoproject)anita@compu:~$ pip install django~=2.2
```

Ahora sí, si el output de la terminal no les devuelve ningún error, pueden seguir las instrucciones de la [sección 1 del tutorial de Django](https://docs.djangoproject.com/es/2.2/intro/tutorial01/) (siempre dentro del entorno virtual).    
¡Adelante!    

### Tutorial para LXC en Ubuntu
Les comparto un recurso que utilizó uno de les alumnes para montar un contenedor y desplegar allí su proyecto de Django.
http://blog.jorgeivanmeza.com/2015/10/los-primeros-10-minutos-con-lxc-en-ubuntu/

### Ventaja de los contenedores
También nos pasó una presentación donde sintetiza las ventajas de trabajar con contenedores LXC en lugar de utilizar máquinas virtuales. Se los recomiendo para quienes estén trabajando con múltiples proyectos en una misma computadora y especialmente para mejorar sus conocimientos acerca de cómo diseñar sus entornos de desarrollo. (Archivo descargable)
