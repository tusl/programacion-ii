## Software para control de cambios. Qué son y cómo utilizar repositorios.    

En este último módulo vamos a aprovechar la existencia de repositorios públicos con dos objetivos fundamentales:    

- Analizar proyectos desarrollados en Django;    
- Utilizar software de control de versiones para compartir nuestro código y/o reutilizar código libre desarrollado por otras personas.    

Con este módulo cerramos los contenidos previstos para esta materia y abrimos la puerta para que comiencen a reutilizar, compartir y participar en la comunidad de desarrollo.    
Si ya tienen conocimientos sobre el uso de repositorios y software git, esta unidad va a resultarles muy simple porque los contenidos son muy elementales. Al igual que siempre, les animo a compartir sus experiencias y conocimientos en los Foros.    
Como actividad de cierre les propongo un trabajo integrador que servirá para regularizar la materia y avanzar en el desarrollo para el examen final.    



