## Modelos y consultas avanzadas en Django
Recuperando algunas de las nociones conceptuales sobre Programación Orientada a Objetos que vimos en el tema 4, vamos a analizar cómo funcionan los modelos y por qué son importantes para entender el funcionamiento de la API de Django para consultas a la base de datos.    

En este tema hice una adaptación de la documentación oficial de Django, debido a que no he encontrado los mismos contenidos en español. No es una traducción, sino una adaptación, pero creo que les será de mucha utilidad.    

### Modelos
Para poder realizar consultas avanzadas utilizando el ORM de Django primero debemos tener bien en claro qué son los modelos.    
Un modelo es una fuente de información definida y simple sobre tus datos. Contiene los campos indispensables y los comportamientos de los datos que estás guardando. Por lo general, cada modelo mapea a una sola tabla de la base de datos.    
Aspectos básicos:    

* Cada modelo es una clase de Python que se comporta como subclase de django.db.models.Model    
* Cada atributo del modelo representa un campo de la base de datos    
* Con todo esto, Django te ofrece una API para acceder a la base de datos generada de manera automática.    

#### Ejemplo de un modelo
Supongamos que en nuestra base de datos vamos a tener un listado de distintas personas. Podemos definir un modelo general **Persona** con los campos "nombre" y "apellido":    

```
from django.db import models
class Persona(models.Model):
    nombre = models.ChasField(max_length=30)
    apellido = models.ChasField(max_length=30)
```

Por lo tanto, "nombre" y "apellido" son los **campos** del modelo. Cada campo se constituye como **atributo de clase** y cada atributo mapea a una columna específica de la base de datos.    
Si traducimos el ejemplo a SQL, el modelo **Persona** crearía una tabla en la base de datos parecida a la siguiente:   

```
CREATE TABLE myapp_person (
    "id" serial NOT NULL PRIMARY KEY,
    "nombre" varchar(30) NOT NULL,
    "apellido" varchar(30) NOT NULL);
```

#### Cómo usar el modelo
Una vez que hayas definido tus modelos, necesitás avisarle a Django que vas a utilizarlos. Esto se logra editando el archivo de configuración cambiando el contenido de **INSTALLED_APPS** agregando el nombre del módulo con el respectivo _models.py_    
En el ejemplo que usamos anteriormente, el modelo para nuestra aplicación se encuentra en el módulo **myapp.models**, por lo tanto en el listado de **INSTALLED_APPS** debe estar listada "myapp".  

Para entender mejor la relación de los modelos y la base de datos, les dejo este video:    

<iframe src="https://www.youtube.com/embed/F-ZZHySD0mM" allow="autoplay; encrypted-media" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>     

### Campos    

La parte más importante de un modelo es la lista de **campos** de base de datos que dicho modelo define. Los campos son especificados por los **atributos de clase** y pueden ser de varios tipos. Cada uno de estos tipos debe corresponder a una instancia de la clase **Field**. Django usa los tipos de la clase **Field** para determinar algunas cosas:    

* La columna del tipo de campo, que indica a la base de datos qué tipo de datos guardar (por ejemplo INTEGER, VARCHAR, TEXT).    
* El widget de HTML que se utilizará por defecto para renderizar un formulario de campo (por ejemplo `input type="text"`, `select`).     
* Los mínimos requerimientos de validación, utilizados en el admin de Django y en los formularios autogenerados.     

Si bien Django provee muchos tipos de campos incluidos en el framework, en caso de ser necesario se pueden definir campos personalizados para determinados modelos.    

#### Opciones del campo    

Cada campo necesita de un set de argumentos específicos a dicho campo. Por ejemplo, vimos que en la clase **Persona** seteamos el atributo _nombre_ como **CharField** (una subclase) que requiere un _max_length_ como argumento. Este argumento va a especificarle a la base de datos el tamaño del VARCHAR, que sería el espacio en caracteres necesario para guardar el dato.    
Además, existen otras opciones de campo que suelen utilizarse tanto para validación como para relación en base de datos. Los tres más utilizados son:    

* **null**: si está definido como **True**, Django interpretará que los valores vacíos se pueden guardar como NULL en la base de datos. Por default esta opción está definida como **False**.    
* **blank**: si está definido como **True**, el campo puede estar en blanco. El default es **False**.    
* **choices**: genera una lista de tuplas que en el widget web habilitarán una selección limitada de opciones (tipo check box) en lugar de dejar un campo de texto en blanco.    

La diferencia entre **null** y **blank** es sobre todo de validación. Si <code>`blank=True`</code> el formulario va a aceptar campos vacíos. En cambio, si <code>`blank=False`</code>, estamos definiendo un campo como obligatorio. **null** está relacionado específicamente con la base de datos, ya que puede devolver ese valor si el campo está vacío.    
Para entender mejor cómo funcionan estas opciones de campo, les dejo este video (y les pido disculpas por el Windows, pero es valiosa la demostración). Además, es un adelanto para que puedan entender cómo funcionan las migraciones en Django.    

<iframe src="https://www.youtube.com/embed/NI8UJ8P7oWI" allow="autoplay; encrypted-media" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>    

### Cómo hacer queries    

Una vez que diseñamos los modelos, Django automáticamente nos provee una API que nos permite crear, recuperar, actualizar o borrar objetos. Para entender mejor cómo realizar consultas avanzadas con esta API, vamos a seguir el ejemplo provisto por el [tutorial de DjangoGirls](https://tutorial.djangogirls.org/es/django_orm/), donde se hace referencia a una webapp muy simple que consiste en un blog.    

En caso que sepan leer en inglés, pueden complementar esta lectura con la documentación oficial de Django referida a esta API, aunque no es obligatorio para esta unidad.     

### Migraciones    

Son la forma en que Django propaga los cambios que hacemos en los modelos (agregar un campo, borrar un modelo, etc.) en el esquema de la base de datos. Están diseñados para ser automáticos, pero es necesario saber cuándo hacer migraciones, cuando correrlas y los problemas más comunes con los que nos podemos encontrar.   

#### Los comandos

* **migrate**, es el responsable de aplicar o revertir las magraciones    
* **makemigrations**, que es el responsable de crear nuevas migraciones basadas en los cambios que hicimos en los modelos    
*  **sqlmigrate**, que muestra las sentencias SQL para una migración    
*  **showmigrations**, que lista las migraciones de un proyecto y su status.    

(Para las actividades que desarrollamos en esta materia, utilizaremos principalmente los dos primeros comandos, pero es bueno que sepan que existen estos cuatro.)    
Las **migraciones** pueden entenderse como un sistema de control de versiones (ya vamos a conocer mejor de qué se tratan estos sistemas en el próximo tema) para el esquema de la base de datos: **makemigrations** empaqueta los cambios en los modelos dentro de una serie de archivos de migración y **migrate** es el responsable de aplicar esos cambios en tu base de datos.    

#### Flujo de trabajo de las migraciones     

Para entender cómo funcionan estos comandos, podemos probar lo siguiente: realizar cambios en los modelos (agregar un cambio, eliminar una parte del modelo, etc.) y luego aplicar el **makemigrations**.     
Esto quiere decir que los modelos se van a escanear y comparar con las versiones que contienen los archivos de migración y por ende se van a ejecutar una nueva serie de migraciones. Una vez que chequeamos que los cambios que indica Django coinciden con lo que hicimos, aplicamos los cambios en la base de datos utilizando el comando **migrate**.    

